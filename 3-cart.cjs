const products = [
  {
    shampoo: {
      price: "$50",
      quantity: 4,
    },
    "Hair-oil": {
      price: "$40",
      quantity: 2,
      sealed: true,
    },
    comb: {
      price: "$12",
      quantity: 1,
    },
    utensils: [
      {
        spoons: { quantity: 2, price: "$8" },
      },
      {
        glasses: { quantity: 1, price: "$70", type: "fragile" },
      },
      {
        cooker: { quantity: 4, price: "$900" },
      },
    ],
    watch: {
      price: "$800",
      quantity: 1,
      type: "fragile",
    },
  },
];

/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/

function formattedData(products) {

  const result = products.map((product) => {

    const keys = Object.keys(product);

    const obj = keys.reduce((newObj, key) => {

      if (Array.isArray(product[key])) {

        const correctedObj = product[key].reduce((innerObj, element) => {

          const newElement = {};
          newElement[element] = product[key][element];

          return { ...innerObj, ...element };
        });

        return { ...newObj, ...correctedObj };

      } else {

        const newElement = {};
        newElement[key] = product[key];

        return { ...newObj, ...newElement };
      }
    }, {});

    return obj;
  });

  return result;
}

const result = formattedData(products);





// Solution 1

function itemsWithPriceMoreThan(products, price) {

  const filteredObj = products.map((product) => {

    const productKeys = Object.entries(product);

    const filtered = productKeys.filter((productKey) => {
      // console.log(productKey[1].price);
      const productPrice = +productKey[1].price.replace("$", "");
      // console.log(productPrice);
      return productPrice > price;
    });
    // console.log(filtered);
    return Object.fromEntries(filtered);
  });

  return filteredObj;
}

const solution1 = itemsWithPriceMoreThan(result, 65);

console.log(solution1);




// Solution 2

function itemsWithOrdersMoreThan(products, quantity) {

  const filteredObj = products.map((product) => {

    const productKeys = Object.entries(product);

    const filtered = productKeys.filter((productKey) => {

      const productPrice = +productKey[1].quantity;

      return productPrice > quantity;
    });

    return Object.fromEntries(filtered);
  });

  return filteredObj;
}

const solution2 = itemsWithOrdersMoreThan(result, 1);

console.log(solution2);






// Solution 3

function itemsWithFragileType(products) {

  const filteredObj = products.map((product) => {

    const productKeys = Object.entries(product);

    const filtered = productKeys.filter((productKey) => {

      if (productKey[1].type !== undefined && productKey[1].type === "fragile") {

        return true;
      }

    });

    return Object.fromEntries(filtered);
  });

  return filteredObj;
}

const solution3 = itemsWithFragileType(result);

console.log(solution3);





//  Solution 4

function leastAndMostExpensive(products) {

  const filteredObj = products.map((product) => {

    const productKeys = Object.entries(product);

    const sorted = productKeys.sort((productKey1, productKey2) => {

      const product1MRP = +productKey1[1].price.replace("$", "") / productKey1[1].quantity;
      const product2MRP = +productKey2[1].price.replace("$", "") / productKey2[1].quantity;

      return product1MRP - product2MRP;
    });

    return sorted;
  });

  return [filteredObj[0][0], filteredObj[0][filteredObj[0].length - 1]];
}

const solution4 = leastAndMostExpensive(result);


console.log(Object.fromEntries(solution4));




// Solution 5

function groupingItemsOnTheBasisOfStateOfMatter(products) {

  const groupedItems = products.map((product) => {

    const productEntries = Object.entries(product);

    const reducedItems = productEntries.reduce((newObj, productKey) => {

      // console.log(productKey);
      if (productKey[0] === "shampoo" || productKey[0] === "Hair-oil") {

        if(newObj.liquid === undefined){

          newObj = { ...newObj, liquid: [productKey] };
        } else {

          // console.log(newObj.liquid);
          liquid = [...newObj.liquid];
  
          newObj = { ...newObj, liquid: [...liquid, productKey] };
        }
        
      } else if (

        productKey[0] === "comb" || productKey[0] === "spoons" || productKey[0] === "glasses" || productKey[0] === "cooker" || productKey[0] === "watch") {
         
          if(newObj.solid === undefined){

            newObj = { ...newObj, solid: [productKey] };

          } else {
            // console.log(newObj.solid);
            solid = [...newObj.solid];
    
            newObj = { ...newObj, solid: [...solid, productKey] };
          }
      }
     
      return newObj;

    }, {});
    
    return reducedItems;
  });

  return groupedItems;
}

const solution5 = groupingItemsOnTheBasisOfStateOfMatter(result);

console.log(solution5);
